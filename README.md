# **L’impact de l’E-commerce sur l’économie**


+ 56% des internautes réservent leur voyage sur internet
+ 45% achètent des produits culturels et des vêtements sur le web
+ 12% ont recours à cet outil du quotidien pour faire des courses alimentaires

Tout ceci va en s’améliorant. C’est dire à quel point Internet comble la plupart de nos besoins les plus basiques ainsi que nos intentions d’achat.

Les économies effectuées par le consommateur lorsqu’il achète en ligne constituent pour lui une seconde source de valeur induite par internet. Autrement dit, les achats que l’on réalise sur internet (sur une échelle de 150 produits) sont 10% moins chers que ceux du magasin.

Internet crée des secteurs d’activité au complet. Ils représentent une source intarissable de lutte contre le chômage soit 25% de la croissance et de la création nette d’emplois.

Des grandes institutions scolaires ont mis en place tout un cursus de formations.

Le but est ainsi de développer ce secteur qui apparaît comme étant générateur des métiers d’avenir.

![ecommerce](https://www.fevad.com/wp-content/uploads/2019/06/4-EEC19_CHIFFRES_CLES.jpg)

Ces chiffres fiables et surtout très récents montrent l’impact de l’E-commerce sur l’économie française cependant selon l’IDC (International Data Corporation) qui est un fournisseur d’informations sur le marché croit aux marges de progression pour exploiter pleinement le potentiel numérique de la France notamment l’augmentation de 12% du panier moyen e-commerce, l’augmentation de la pénétration haut débit de 62% dans les prochaines années.

Les bonnes pratiques en Europe et dans le monde dont la France pourrait s’inspirer sont :

le développement des usages et services numériques tant dans le secteur privé que public le soutien des PME innovantes et de la recherche pour favoriser le développement du numérique.





# **![net](images/network-internet-icon.png)[Chronologie du réseau internet](https://www.tiki-toki.com/timeline/entry/137139/Chronologie-du-rseau-internet/#vars!date=1955-04-14_07:09:14!)**
# **![net](images/network-internet-icon.png)[Qu'est-ce qu'internet?](https://www.youtube.com/watch?v=Dxcc6ycZ73M&feature=youtu.be)**
# **![net](images/network-internet-icon.png)[WEB : Un service internet](https://www.youtube.com/watch?v=kBXQZMmiA4s)**
+ [La première page WEB !!](http://info.cern.ch/hypertext/WWW/TheProject.html)

# **![projet](images/gestiondeprojet.png) Réaliser trois articles(Format texte)**

+ Une chronologie du réseau internet.
+ Un article sur le réseau internet.
+ Un article sur le service WEB d'internet.